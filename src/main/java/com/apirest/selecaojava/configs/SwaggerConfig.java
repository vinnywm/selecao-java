package com.apirest.selecaojava.configs;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Classe que configura a utilização do Swagger 2.
 * @author Vinicius
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket selecaoApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.apirest.selecaojava"))
				.paths(regex("/api.*"))
				.build()
				.apiInfo(metaInfo());
	}
	
	private ApiInfo metaInfo() {
		ApiInfo apiInfo = new ApiInfo(
				"Seleção INDRA API REST",
				"API de Usuário, Histórico de Preço de Combustível, etc.",
				"1.0",
				"Terms of Service",
				new Contact("Vinícius Abreu", "https://www.linkedin.com/in/viniciusabreufranca/",
							"viniciusabreufranca@yahoo.com"),
				"Apache License Version 2.0",
				"https://www.apache.org/licensen.html",
				new ArrayList<VendorExtension>()
				);
		return apiInfo;
	}
}
