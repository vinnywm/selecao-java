package com.apirest.selecaojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelecaoJavaApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SelecaoJavaApplication.class, args);

	}

}
