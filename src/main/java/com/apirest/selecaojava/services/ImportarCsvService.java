package com.apirest.selecaojava.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apirest.selecaojava.models.HistoricoDePreco;
import com.apirest.selecaojava.repositories.HistoricoDePrecoRepository;
import com.apirest.selecaojava.utils.ImportaCsvUtil;

@Service
public class ImportarCsvService {

	@Autowired
	HistoricoDePrecoRepository historicoDePrecoRepository;
	
	
	public void importarCsv() {
		List<HistoricoDePreco> lhp =  ImportaCsvUtil.importaCsvParaBancoDeDados("csv/2018-1_CopiaUm.csv");
		for (HistoricoDePreco historicoDePreco : lhp) {
			historicoDePrecoRepository.save(historicoDePreco);
		}
		System.out.println("IMPORTACAO CONCLUIDA");
	}
}
