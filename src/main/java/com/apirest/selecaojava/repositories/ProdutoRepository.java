package com.apirest.selecaojava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apirest.selecaojava.beans.ProdutoAgregadoBean;
import com.apirest.selecaojava.models.Estado;
import com.apirest.selecaojava.models.Produto;

/**
 * Interface de ProdutoRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long>{

	Produto findById(long id);
	@Query("SELECT new com.apirest.selecaojava.beans.ProdutoAgregadoBean("
			+ "p.nome as nomeProduto, AVG(p.valorCompra) as mediaValorCompra, AVG(p.valorVenda) as mediaValorVenda,"
			+ "m.nome as municipio, m.estado as estadoSigla) "
			+ "FROM Produto p\r\n" + 
			"INNER JOIN Revenda r ON p.id = r.produto.id\r\n" + 
			"INNER JOIN Municipio m ON m.id = r.municipio.id\r\n" + 
			"WHERE m.nome = :municipio AND m.estado = :estado"
			+ " GROUP BY p.nome")
	public List<ProdutoAgregadoBean> mediaProdutoMunicipio(@Param("estado") Estado estado, @Param("municipio") String municipio);
}
