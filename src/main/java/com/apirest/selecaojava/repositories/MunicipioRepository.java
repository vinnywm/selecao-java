package com.apirest.selecaojava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apirest.selecaojava.beans.MunicipioAgregadoBean;
import com.apirest.selecaojava.models.Municipio;

/**
 * Interface de MunicipioRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface MunicipioRepository extends JpaRepository<Municipio, Long>{

	Municipio findById(long id);
	
	@Query("SELECT new com.apirest.selecaojava.beans.MunicipioAgregadoBean("
			+ "h.revenda.municipio.nome as municipio, AVG(h.revenda.produto.valorCompra) as mediaValorCompra, "
			+ "AVG(h.revenda.produto.valorVenda) as mediaValorVenda) "
			+ "FROM HistoricoDePreco h \r\n"
			+ " GROUP BY h.revenda.municipio.nome")
	List<MunicipioAgregadoBean> mediaPrecoCompraVendaPorMunicipio();
}