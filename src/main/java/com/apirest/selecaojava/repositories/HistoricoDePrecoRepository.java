package com.apirest.selecaojava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apirest.selecaojava.models.HistoricoDePreco;
import com.apirest.selecaojava.models.Regiao;

/**
 * Interface de HistoricoDePrecoRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface HistoricoDePrecoRepository extends JpaRepository<HistoricoDePreco, Long>{

	HistoricoDePreco findById(long id);
	
	@Query("SELECT h FROM HistoricoDePreco h GROUP BY h.revenda.id ORDER BY h.revenda.nome")
	List<HistoricoDePreco> listaHistoricoPorDistribuidora();

	@Query("SELECT h FROM HistoricoDePreco h ORDER BY h.dataDaColeta DESC")
	List<HistoricoDePreco> listaHistoricoPorDataDaColeta();
	
	@Query("SELECT h FROM HistoricoDePreco h ORDER BY h.dataDaColeta")
	List<HistoricoDePreco> listaHistoricoPorDataDaColetaASC();


	@Query("SELECT h FROM HistoricoDePreco h WHERE h.revenda.municipio.regiao = :regiao")
	List<HistoricoDePreco> listaHistoricoDePrecosPorRegiao(@Param("regiao") Regiao regiao);
}
