package com.apirest.selecaojava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apirest.selecaojava.beans.BandeiraAgregadoBean;
import com.apirest.selecaojava.models.Bandeira;

/**
 * Interface de BandeiraRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface BandeiraRepository extends JpaRepository<Bandeira, Long>{

	Bandeira findById(long id);

	@Query("SELECT new com.apirest.selecaojava.beans.BandeiraAgregadoBean("
			+ "h.bandeira.nome as nomeBandeira, AVG(h.revenda.produto.valorCompra) as mediaValorCompra) "
			+ "FROM HistoricoDePreco h \r\n"
			+ " GROUP BY h.bandeira.nome")
	List<BandeiraAgregadoBean> mediaPrecoCompraPorBandeira();
}
