package com.apirest.selecaojava.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apirest.selecaojava.models.Revenda;

/**
 * Interface de RevendaRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface RevendaRepository extends JpaRepository<Revenda, Long>{

	Revenda findById(long id);
}