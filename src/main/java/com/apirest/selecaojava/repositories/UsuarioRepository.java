package com.apirest.selecaojava.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apirest.selecaojava.models.Usuario;

/**
 * Interface de UsuarioRepository.
 * Amplia comportamentos do repositório JPA padrão.
 * --------------------------------------------------
 * @author Vinicius
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

	Usuario findById(long id);
}
