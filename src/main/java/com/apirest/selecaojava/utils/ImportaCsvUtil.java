package com.apirest.selecaojava.utils;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.apirest.selecaojava.models.Bandeira;
import com.apirest.selecaojava.models.Estado;
import com.apirest.selecaojava.models.HistoricoDePreco;
import com.apirest.selecaojava.models.Municipio;
import com.apirest.selecaojava.models.Produto;
import com.apirest.selecaojava.models.Regiao;
import com.apirest.selecaojava.models.Revenda;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

/**
 * Classe utilitária que realiza a leitura de um arquivo CSV e tranforma
 * os dados em objetos beans prontos para serem gerenciados.
 * ---------------------------------------------------------------------
 * @author Vinicius
 *
 */
public class ImportaCsvUtil {



	/**
	 * Aceita um path de onde está o arquivo CSV e o retorna em uma Lista
	 * pronta de Histórico de Preços.
	 * @param path Caminho do arquivo CSV
	 * @return Uma lista de HistoricoDePreco
	 */
	@SuppressWarnings("unused")
	public static List<HistoricoDePreco> importaCsvParaBancoDeDados(String path) {
		List<HistoricoDePreco> historicoList = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Municipio municipio;
			Revenda revenda;
			Produto produto;
			Bandeira bandeira;
			String dataDaColeta;
			HistoricoDePreco historicoDePreco;
			List<String[]> allData = lerCsvCompleto(path);
			historicoList = new ArrayList<HistoricoDePreco>();

			for (String[] coluna : allData) {
				dataDaColeta	= 	coluna[6];
				bandeira		= 	new Bandeira(coluna[10]);
				municipio		= 	new Municipio(coluna[2], Estado.valueOf(coluna[1]), Regiao.valueOf(coluna[0]));
				produto			= 	new Produto(coluna[5], coluna[9], coluna[8], coluna[7]);
				revenda			=	new Revenda(coluna[3], coluna[4], municipio, produto);
				historicoDePreco = new HistoricoDePreco(revenda, bandeira, sdf.parse(dataDaColeta));
				

				historicoList.add(new HistoricoDePreco(revenda, bandeira, sdf.parse(dataDaColeta)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return historicoList;
	}

	/**
	 * Retorna o CSV lido em uma lista de array de Strings.
	 * @param path caminho para o arquivo
	 * @return Uma Lista de array de strings
	 * @throws Exception Lança um exceção genérica
	 */
	private static List<String[]> lerCsvCompleto(String path) throws Exception {
		try {
			Reader reader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource(path).toURI()));
			CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader csvReader = new CSVReaderBuilder(reader).withCSVParser(parser).build();

			return csvReader.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
