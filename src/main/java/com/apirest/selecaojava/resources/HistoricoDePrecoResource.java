package com.apirest.selecaojava.resources;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apirest.selecaojava.beans.BandeiraAgregadoBean;
import com.apirest.selecaojava.beans.MunicipioAgregadoBean;
import com.apirest.selecaojava.beans.ProdutoAgregadoBean;
import com.apirest.selecaojava.exceptions.BadRequestException;
import com.apirest.selecaojava.exceptions.ResourceNotFoundException;
import com.apirest.selecaojava.models.Bandeira;
import com.apirest.selecaojava.models.Estado;
import com.apirest.selecaojava.models.HistoricoDePreco;
import com.apirest.selecaojava.models.Municipio;
import com.apirest.selecaojava.models.Regiao;
import com.apirest.selecaojava.models.Revenda;
import com.apirest.selecaojava.repositories.BandeiraRepository;
import com.apirest.selecaojava.repositories.HistoricoDePrecoRepository;
import com.apirest.selecaojava.repositories.MunicipioRepository;
import com.apirest.selecaojava.repositories.ProdutoRepository;
import com.apirest.selecaojava.repositories.RevendaRepository;
import com.apirest.selecaojava.utils.ImportaCsvUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Resource que representa o acesso a camada de persistência realizando o CRUD da entidade HistoricoDePreco
 * e outras funcionalidades adicionais.
 * -----------------------------------------------------------------------------------------
 * @author Vinicius
 *
 */
@RestController
@RequestMapping(value="/api")
@Api(value="API REST Histórico de Preços")
@CrossOrigin(origins = "*")
public class HistoricoDePrecoResource {

	@Autowired
	private HistoricoDePrecoRepository historicoDePrecoRepository;
	@Autowired
	private BandeiraRepository bandeiraRepository;
	@Autowired
	private RevendaRepository revendaRepository;
	@Autowired
	private ProdutoRepository produtoRepository;
	@Autowired
	private MunicipioRepository municipioRepository;
	
	/**
	 * A lista de histórico de preços.
	 * @return A lista de histórico de preços.
	 */
	@GetMapping("/historicoDePrecos")
	@ApiOperation(value="Retorna uma lista com históricos de preços")
	public List<HistoricoDePreco> listaHistoricoDePrecos() {
		return historicoDePrecoRepository.findAll();
	}
	
	/**
	 * Retorna um histórico de preço pelo ID
	 * @param id Número identificador a ser buscado
	 * @return Um objeto HistoricoDePreco
	 */
	@GetMapping("/historicoDePreco/{id}")
	@ApiOperation(value="Retorna um histórico de preço pelo ID")
	public HistoricoDePreco listaHistoricoDePreco(@PathVariable(value="id") long id) {
		HistoricoDePreco historicoDePreco = historicoDePrecoRepository.findById(id);
		if (historicoDePreco == null)
			throw new ResourceNotFoundException("Histórico não encontrado para o ID: " + id);
		return historicoDePreco;
	}

	/**
	 * Cria um novo Histórico de Preco
	 * @param historicoDePreco Histórico de preço a ser criado.
	 * @return O histórico de preço criado
	 * @throws BadRequestException Caso aconteça algum problema vindo da view essa exceção será lançada.
	 */
	@PostMapping("/historicoDePreco")
	@ApiOperation(value="Cria um novo Histórico de Preco")
	public HistoricoDePreco salvaHistoricoDePreco(@RequestBody HistoricoDePreco historicoDePreco) throws BadRequestException {
		try {
			
			if (historicoDePreco.getBandeira() == null || historicoDePreco.getBandeira().getNome() == null)
				throw new BadRequestException("Histórico de Preço não contem Bandeira na requisição. Bandeira é NULL ou é um objeto vazio.");
			if (historicoDePreco.getRevenda() == null)
				throw new BadRequestException("Histórico de Preço não contem Revenda na requisição. Revenda é NULL ou é um objeto vazio.");
			else {
				//VERIFICA SE ENTIDADE jÁ EXISTE, SE NÃO EXISTIR SALVA UMA NOVA, SE EXISTIR PEGA A ENTIDADE GERENCIADA.
				Example<Revenda> exampleRevenda = Example.of(historicoDePreco.getRevenda(), ExampleMatcher.matchingAll().withIgnorePaths("id"));
				if (!revendaRepository.exists(exampleRevenda))
					revendaRepository.save(historicoDePreco.getRevenda());
				else
					historicoDePreco.setRevenda((revendaRepository.findOne(exampleRevenda)).get());
			}
			if (historicoDePreco.getDataDaColeta() == null)
				throw new BadRequestException("Histórico de Preço não contem DataDeColeta na requisição. DataDeColeta é NULL.");
			
			return historicoDePrecoRepository.save(historicoDePreco);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Altera um HistoricoDePreco já existente
	 * @param historicoDePreco Objeto a ser alterado.
	 * @return Um Histórico de preço editado.
	 */
	@PutMapping("/historicoDePreco")
	@ApiOperation(value="Altera um HistoricoDePreco já existente")
	public HistoricoDePreco atualizaHistoricoDePreco(@RequestBody HistoricoDePreco historicoDePreco) {
		try {

			if (historicoDePreco.getBandeira() == null || historicoDePreco.getBandeira().getNome() == null)
				throw new BadRequestException("Histórico de Preço não contem Bandeira na requisição. Bandeira é NULL ou é um objeto vazio.");
			else {
				// VERIFICA SE ENTIDADE jÁ EXISTE, SE NÃO EXISTIR SALVA UMA NOVA, SE EXISTIR
				// PEGA A ENTIDADE GERENCIADA.
				Example<Bandeira> exampleBandeira = Example.of(historicoDePreco.getBandeira(),
						ExampleMatcher.matchingAll().withIgnorePaths("id"));
				if (!bandeiraRepository.exists(exampleBandeira))
					bandeiraRepository.save(historicoDePreco.getBandeira());
				else
					historicoDePreco.setBandeira((bandeiraRepository.findOne(exampleBandeira)).get());
			}
			if (historicoDePreco.getRevenda() == null)
				throw new BadRequestException("Histórico de Preço não contem Revenda na requisição. Revenda é NULL ou é um objeto vazio.");
			else {
				// VERIFICA SE ENTIDADE jÁ EXISTE, SE NÃO EXISTIR SALVA UMA NOVA, SE EXISTIR
				// PEGA A ENTIDADE GERENCIADA.
				Example<Revenda> exampleRevenda = Example.of(historicoDePreco.getRevenda(),
						ExampleMatcher.matchingAll().withIgnorePaths("id"));
				if (!revendaRepository.exists(exampleRevenda))
					revendaRepository.save(historicoDePreco.getRevenda());
				else
					historicoDePreco.setRevenda((revendaRepository.findOne(exampleRevenda)).get());
			}
			if (historicoDePreco.getDataDaColeta() == null)
				throw new BadRequestException(
						"Histórico de Preço não contem DataDeColeta na requisição. DataDeColeta é NULL.");

			municipioRepository.save(historicoDePreco.getRevenda().getMunicipio());
			produtoRepository.save(historicoDePreco.getRevenda().getProduto());
			revendaRepository.save(historicoDePreco.getRevenda());
			return historicoDePrecoRepository.save(historicoDePreco);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Deleta um Histórico de Preço.
	 * @param historicoDePreco Objeto a ser deletado.
	 */
	@DeleteMapping("/historicoDePreco")
	@ApiOperation(value="Deleta um Histórico de Preço.")
	public void deletaHistoricoDePreco(@RequestBody HistoricoDePreco historicoDePreco) {
		historicoDePrecoRepository.delete(historicoDePreco);
	}
	
	/**
	 * Importa uma lista com históricos de preços de um aquivo em CSV
	 */
	@GetMapping("/historicoDePrecos/importar")
	@ApiOperation(value="Importa uma lista com históricos de preços de um aquivo em CSV")
	public void importarCsv() {
		List<HistoricoDePreco> lhp =  ImportaCsvUtil.importaCsvParaBancoDeDados("csv/2018-1_Amostra2.csv");
		for (HistoricoDePreco historicoDePreco : lhp) {
			
			Example<Bandeira> exampleBandeira = Example.of(historicoDePreco.getBandeira(), ExampleMatcher.matchingAll().withIgnorePaths("id"));
			Example<Municipio> exampleMunicipio = Example.of(historicoDePreco.getRevenda().getMunicipio(), ExampleMatcher.matchingAll().withIgnorePaths("id"));
			boolean bandeiraExiste = bandeiraRepository.exists(exampleBandeira);
			boolean municipioExiste = municipioRepository.exists(exampleMunicipio);
			//GARANTE INSERCAO UNICA DOS DADOS DE BANDEIRA
			if (!bandeiraExiste) {
				bandeiraRepository.save(historicoDePreco.getBandeira());
			} else {
				Optional<Bandeira> o = bandeiraRepository.findOne(exampleBandeira);
				historicoDePreco.setBandeira(o.get());
			}
			//GARANTE INSERCAO UNICA DOS DADOS DE MUNICIPIO
			if (!municipioExiste) {
				municipioRepository.save(historicoDePreco.getRevenda().getMunicipio());
			} else {
				Optional<Municipio> o = municipioRepository.findOne(exampleMunicipio);
				historicoDePreco.getRevenda().setMunicipio(o.get());
			}
			
			municipioRepository.save(historicoDePreco.getRevenda().getMunicipio());
			produtoRepository.save(historicoDePreco.getRevenda().getProduto());
			revendaRepository.save(historicoDePreco.getRevenda());
			historicoDePrecoRepository.save(historicoDePreco);
			System.out.println(historicoDePreco);
		}
		System.out.println("IMPORTACAO CONCLUIDA");
	}
	
	/**
	 * Lista a média de preço de combustível com base no município.
	 * @param estado Sigla do Estado
	 * @param municipio Nome do Município
	 * @return Uma lista com os produtos e a média de preços por município. 
	 */
	@GetMapping("/historicoDePrecos/mediaPrecoPorMunicipio/{estado}/{municipio}")
	@ApiOperation(value="Lista a média de preço de combustível com base no município.")
	public List<ProdutoAgregadoBean> mediaDePrecoPorMunicipio(@PathVariable(value="estado") String estado, @PathVariable(value="municipio") String municipio) {
		return produtoRepository.mediaProdutoMunicipio(Estado.valueOf(estado), municipio);		
	}
	
	/**
	 * Lista dados com base na região.
	 * @param regiao Sigla da Regiao
	 * @return Uma lista com os dados apenas da região especificada. 
	 */
	@GetMapping("/historicoDePrecos/HistoricoPrecoPorRegiao/{regiao}")
	@ApiOperation(value="Lista os dados apenas da região especificada.")
	public List<HistoricoDePreco> listaHistoricoDePrecosPorRegiao(@PathVariable(value="regiao") String regiao) {
		if(!EnumUtils.isValidEnum(Regiao.class, regiao))
			throw new ResourceNotFoundException("Região não encontrada para o valor: " + regiao.toUpperCase());
		return historicoDePrecoRepository.listaHistoricoDePrecosPorRegiao(Regiao.valueOf(regiao));
	}
	
	/**
	 * Lista o valor médio de compra por bandeira.
	 * @return Uma lista o valor médio de compra de cada bandeira. 
	 */
	@GetMapping("/historicoDePrecos/mediaPrecoCompraPorBandeira")
	@ApiOperation(value="Lista a média de preço de compra por bandeira.")
	public List<BandeiraAgregadoBean> mediaPrecoCompraPorBandeira() {
		return bandeiraRepository.mediaPrecoCompraPorBandeira();		
	}
	
	/**
	 * Lista o valor médio de compra e valor médio de venda por município.
	 * @return Uma lista o valor médio de compra e de venda por município. 
	 */
	@GetMapping("/historicoDePrecos/mediaPrecoCompraVendaMunicipio")
	@ApiOperation(value="Lista a média de preço de compra e venda por município.")
	public List<MunicipioAgregadoBean> mediaPrecoCompraVendaMunicipio() {
		return municipioRepository.mediaPrecoCompraVendaPorMunicipio();		
	}
	
	/**
	 * Lista histórico agrupado por distribuidora e ordenado pelo nome da distribuidora.
	 * @return Uma lista de Histórico de preço
	 */
	@GetMapping("/historicoDePrecos/agrupadosPorDistribuidora")
	@ApiOperation(value="Lista histórico agrupado por distribuidora e ordenado pelo nome da distribuidora.")
	public List<HistoricoDePreco> listaHistoricoPorDistribuidora() {
		return historicoDePrecoRepository.listaHistoricoPorDistribuidora();		
	}

	/**
	 * Lista histórico por data da coleta em ASCENDENTE OU DESCENDENTE.
	 * @param ordem Se DESC lista do mais recente para o último, se qualquer ou coisa lista em ASC, do último para o mais recente.
	 * @return Uma lista de Histórico de preço
	 */
	@GetMapping("/historicoDePrecos/agrupadosPorDataDaColeta/{ordem}")
	@ApiOperation(value="Lista histórico por data da coleta em ASCENDENTE OU DESCENDENTE."
			+ " As opções são 'ASC' e 'DESC', quaisquer outro padrão fará a consulta"
			+ "retornar os dados em ASC.")
	public List<HistoricoDePreco> listaHistoricoPorDataDaColeta(@PathVariable(value="ordem") String ordem) {
		if (ordem.toUpperCase().equals("DESC"))
			return historicoDePrecoRepository.listaHistoricoPorDataDaColeta();
		else
			return historicoDePrecoRepository.listaHistoricoPorDataDaColetaASC();
	}
	
	

}
