package com.apirest.selecaojava.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apirest.selecaojava.models.Usuario;
import com.apirest.selecaojava.repositories.UsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Resource que representa o acesso a camada de persistência realizando o CRUD da entidade Usuário
 * ---------------------------------------------------------------------------
 * @author Vinicius
 *
 */
@RestController
@RequestMapping(value="/api")
@Api(value="API REST Usuários")
@CrossOrigin(origins = "*")
public class UsuarioResource {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/usuarios")
	@ApiOperation(value="Retorna uma lista de Usuários")
	public List<Usuario> listaUsuarios() {
		return usuarioRepository.findAll();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/usuario/{id}")
	@ApiOperation(value="Retorna um Usuário pelo ID")
	public Usuario listaUsuario(@PathVariable(value="id") long id) {
		return usuarioRepository.findById(id);
	}

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	@PostMapping("/usuario")
	@ApiOperation(value="Cria um novo Usuário")
	public Usuario salvaUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	/**
	 * 
	 * @param usuario
	 * @return
	 */
	@PutMapping("/usuario")
	@ApiOperation(value="Altera um Usuário já existente")
	public Usuario atualizaUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	/**
	 * 
	 * @param usuario
	 */
	@DeleteMapping("/usuario")
	@ApiOperation(value="Retorna uma lista de Usuários")
	public void deletaUsuario(@RequestBody Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
}
