package com.apirest.selecaojava.beans;

public class BandeiraAgregadoBean {

	private String nomeBandeira;
	private double mediaValorCompra;

	public BandeiraAgregadoBean() {}

	public BandeiraAgregadoBean(String nomeBandeira, double mediaValorCompra) {
		super();
		this.nomeBandeira = nomeBandeira;
		this.mediaValorCompra = mediaValorCompra;
	}

	public String getNomeBandeira() {
		return nomeBandeira;
	}

	public void setNomeBandeira(String nomeBandeira) {
		this.nomeBandeira = nomeBandeira;
	}

	public double getMediaValorCompra() {
		return mediaValorCompra;
	}

	public void setMediaValorCompra(double mediaValorCompra) {
		this.mediaValorCompra = mediaValorCompra;
	}


	
}
