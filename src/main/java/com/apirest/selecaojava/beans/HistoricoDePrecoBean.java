package com.apirest.selecaojava.beans;

import com.opencsv.bean.CsvBindByPosition;

public class HistoricoDePrecoBean extends CsvBean {

	@CsvBindByPosition(position = 0)
	private String estadoSigla;
	@CsvBindByPosition(position = 1)
	private String nomeMunicipio;
	@CsvBindByPosition(position = 2)
	private String regiaoSigla;
	@CsvBindByPosition(position = 3)
	private String nomeRevenda;
	@CsvBindByPosition(position = 4)
	private String instalacaoCodigo;
	@CsvBindByPosition(position = 5)
	private String nomeProduto;
	@CsvBindByPosition(position = 6)
	private String dataDaColeta;
	@CsvBindByPosition(position = 7)
	private String valorDeCompra;
	@CsvBindByPosition(position = 8)
	private String valorDeVenda;
	@CsvBindByPosition(position = 9)
	private String unidadeDeMedida;
	@CsvBindByPosition(position = 10)
	private String nomeDaBandeira;
	
	
	
	public String getEstadoSigla() {
		return estadoSigla;
	}
	public void setEstadoSigla(String estadoSigla) {
		this.estadoSigla = estadoSigla;
	}
	public String getNomeMunicipio() {
		return nomeMunicipio;
	}
	public void setNomeMunicipio(String nomeMunicipio) {
		this.nomeMunicipio = nomeMunicipio;
	}
	public String getRegiaoSigla() {
		return regiaoSigla;
	}
	public void setRegiaoSigla(String regiaoSigla) {
		this.regiaoSigla = regiaoSigla;
	}
	public String getNomeRevenda() {
		return nomeRevenda;
	}
	public void setNomeRevenda(String nomeRevenda) {
		this.nomeRevenda = nomeRevenda;
	}
	public String getInstalacaoCodigo() {
		return instalacaoCodigo;
	}
	public void setInstalacaoCodigo(String instalacaoCodigo) {
		this.instalacaoCodigo = instalacaoCodigo;
	}
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public String getDataDaColeta() {
		return dataDaColeta;
	}
	public void setDataDaColeta(String dataDaColeta) {
		this.dataDaColeta = dataDaColeta;
	}
	public String getValorDeCompra() {
		return valorDeCompra;
	}
	public void setValorDeCompra(String valorDeCompra) {
		this.valorDeCompra = valorDeCompra;
	}
	public String getValorDeVenda() {
		return valorDeVenda;
	}
	public void setValorDeVenda(String valorDeVenda) {
		this.valorDeVenda = valorDeVenda;
	}
	public String getUnidadeDeMedida() {
		return unidadeDeMedida;
	}
	public void setUnidadeDeMedida(String unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}
	public String getNomeDaBandeira() {
		return nomeDaBandeira;
	}
	public void setNomeDaBandeira(String nomeDaBandeira) {
		this.nomeDaBandeira = nomeDaBandeira;
	}
	
}
