package com.apirest.selecaojava.beans;

public class MunicipioAgregadoBean {

	private String municipio;
	private double mediaValorCompra;
	private double mediaValorVenda;

	public MunicipioAgregadoBean() {}

	public MunicipioAgregadoBean(String municipio, double mediaValorCompra, double mediaValorVenda) {
		super();
		this.municipio = municipio;
		this.mediaValorCompra = mediaValorCompra;
		this.mediaValorVenda = mediaValorVenda;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public double getMediaValorCompra() {
		return mediaValorCompra;
	}

	public void setMediaValorCompra(double mediaValorCompra) {
		this.mediaValorCompra = mediaValorCompra;
	}

	public double getMediaValorVenda() {
		return mediaValorVenda;
	}

	public void setMediaValorVenda(double mediaValorVenda) {
		this.mediaValorVenda = mediaValorVenda;
	}

	
	

	
}
