package com.apirest.selecaojava.beans;

import com.apirest.selecaojava.models.Estado;

public class ProdutoAgregadoBean {

	private String nomeProduto;
	private double mediaValorCompra;
	private double mediaValorVenda;
	private String municipio;
	private Estado estadoSigla;
	
	
	

	public ProdutoAgregadoBean() {}

	public ProdutoAgregadoBean(String nomeProduto, double mediaValorCompra, double mediaValorVenda,
			String municipio, Estado estadoSigla) {
		super();
		this.nomeProduto = nomeProduto;
		this.mediaValorCompra = mediaValorCompra;
		this.mediaValorVenda = mediaValorVenda;
		this.municipio = municipio;
		this.estadoSigla = estadoSigla;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public double getMediaValorCompra() {
		return mediaValorCompra;
	}

	public void setMediaValorCompra(double mediaValorCompra) {
		this.mediaValorCompra = mediaValorCompra;
	}

	public double getMediaValorVenda() {
		return mediaValorVenda;
	}

	public void setMediaValorVenda(double mediaValorVenda) {
		this.mediaValorVenda = mediaValorVenda;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Estado getEstadoSigla() {
		return estadoSigla;
	}

	public void setEstadoSigla(Estado estadoSigla) {
		this.estadoSigla = estadoSigla;
	}






	
	
	
	
}
