package com.apirest.selecaojava.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe que implementa a entidade: Municipio
 * ----------------------------------------------
 * @author Vinicius
 *
 */
@Entity
public class Municipio implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String nome;
	@Enumerated(EnumType.STRING)
	@Column (length=2)
	private Estado estado;
	@Enumerated(EnumType.STRING)
	@Column (length=2)
	private Regiao regiao;
	
	
	public Municipio() {}
	
	public Municipio(String nome, Estado estado, Regiao regiao) {
		super();
		this.nome = nome;
		this.estado = estado;
		this.regiao = regiao;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
	
}
