package com.apirest.selecaojava.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Classe que implementa a entidade: Bandeira
 * ----------------------------------------------
 * @author Vinicius
 *
 */
@Entity
public class Bandeira implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(nullable=false, unique = true)
	@NotNull
	private String nome;
	
	public Bandeira() {}
	
	public Bandeira(String nome) {
		this.nome = nome;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
