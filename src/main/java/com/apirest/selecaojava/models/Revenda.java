package com.apirest.selecaojava.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Classe que implementa a entidade: Revenda
 * ----------------------------------------------
 * @author Vinicius
 *
 */
@Entity
public class Revenda implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull
	private String nome;
	@NotNull
	private String codigoInstalacao;
	
	@ManyToOne 
	@JoinColumn (name= "municipio_fk", nullable=false)
	private Municipio municipio;
	
	@ManyToOne 
	@JoinColumn (name= "produto_fk", nullable=false)
	private Produto produto;

	
	public Revenda () {}
	
	public Revenda(String nome, String codigoInstalacao, Municipio municipio, Produto produto) {
		super();
		this.nome = nome;
		this.codigoInstalacao = codigoInstalacao;
		this.municipio = municipio;
		this.produto = produto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigoInstalacao() {
		return codigoInstalacao;
	}

	public void setCodigoInstalacao(String codigoInstalacao) {
		this.codigoInstalacao = codigoInstalacao;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	
	
	
	
}
