package com.apirest.selecaojava.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class HistoricoDeCompra implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne
	private Revenda revenda;
	@OneToOne
	private Produto produto;
	@OneToOne
	private Bandeira bandeira;
	
	@Column(nullable=false)
	@Temporal(value=TemporalType.DATE)
	private Date dataDaColeta;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public Date getDataDaColeta() {
		return dataDaColeta;
	}

	public void setDataDaColeta(Date dataDaColeta) {
		this.dataDaColeta = dataDaColeta;
	}
	
	
	
}
