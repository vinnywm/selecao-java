package com.apirest.selecaojava.models;

/**
 * Enumeration que representa as regiões do Brasil
 * ------------------------------------------------
 * @author Vinicius
 *
 */
public enum Regiao {

	CO, N, NE, S, SE
	
}
