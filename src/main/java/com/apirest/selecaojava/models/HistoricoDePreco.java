package com.apirest.selecaojava.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Classe que implementa a entidade: HistóricoDePreço
 * ----------------------------------------------
 * @author Vinicius
 *
 */
@Entity
public class HistoricoDePreco implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne
	@NotNull(message = "Histórico de preço deve conter uma revenda")
	private Revenda revenda;
	@OneToOne
	@NotNull(message = "Histórico de preço deve conter uma bandeira")
	private Bandeira bandeira;
	@Column(nullable=false)
	@Temporal(value=TemporalType.DATE)
	private Date dataDaColeta;
	
	
	
	public HistoricoDePreco() {}
	
	public HistoricoDePreco(Revenda revenda, Bandeira bandeira, Date dataDaColeta) {
		super();
		this.revenda = revenda;
		this.bandeira = bandeira;
		this.dataDaColeta = dataDaColeta;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public Date getDataDaColeta() {
		return dataDaColeta;
	}

	public void setDataDaColeta(Date dataDaColeta) {
		this.dataDaColeta = dataDaColeta;
	}

	@Override
	public String toString() {
		return "HistoricoDePreco [id=" + id + ", revenda=" + revenda + ", bandeira=" + bandeira + ", dataDaColeta="
				+ dataDaColeta + "]";
	}
	
	
	
}
