package com.apirest.selecaojava.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe que implementa a entidade: Produto
 * ----------------------------------------------
 * @author Vinicius
 *
 */
@Entity
public class Produto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String nome;
	private String unidadeDeMedida;
	@Column(precision = 4)
	private BigDecimal valorVenda;
	@Column(precision = 4)
	private BigDecimal valorCompra;
	
	public Produto() {}
	
	public Produto(String nome, String unidadeDeMedida, String valorVenda, String valorCompra) {
		super();
		this.nome = nome;
		this.unidadeDeMedida = unidadeDeMedida;
		this.valorCompra = valorCompra == null? new BigDecimal(0) : valorCompra.isEmpty()? new BigDecimal(0) : new BigDecimal(valorVenda.replace(',', '.'));
		this.valorVenda = valorVenda == null? new BigDecimal(0) : valorVenda.isEmpty()? new BigDecimal(0) : new BigDecimal(valorVenda.replace(',', '.'));
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUnidadeDeMedida() {
		return unidadeDeMedida;
	}
	public void setUnidadeDeMedida(String unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}

	public BigDecimal getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}

	public BigDecimal getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}

	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", unidadeDeMedida=" + unidadeDeMedida + ", valorVenda="
				+ valorVenda + ", valorCompra=" + valorCompra + "]";
	}
	
	
	
	
}
